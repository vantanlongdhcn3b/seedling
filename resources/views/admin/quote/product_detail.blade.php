<div class="row" style="min-width: 250px">
    <div class="col-12">
        <a target="_blank" class="text-limit" href="{{ '/quote/'.$entry->id.'/show' }}">
            <i class="fa fa-eye"></i>
            <span>{{number_format($entry->total_product_number)}} sản phẩm - {{\Carbon\Carbon::parse($entry->created_at)->format('d/m/Y')}}</span>
        </a>
        <div class="text-limit">
            <font size="2">
                <div>Tổng tiền: {{number_format($entry->total_price)}} VNĐ</div>
                <div>Đã chiết khấu ({{ $entry->discount }}%): {{number_format($entry->discounted_price)}} VNĐ</div>
            </font>
        </div>
    </div>
</div>
