<table class="table table-bordered">
    <thead>
    <tr>
        <th style="min-width: 50px;">STT</th>
        <th style="min-width: 200px;">Họ tên</th>
        <th style="min-width: 200px;">Số điện thoại</th>
        <th style="min-width: 200px;">Vai trò</th>
        <th style="min-width: 200px;">Báo giá</th>
        <th style="min-width: 200px;">Tổng sản phẩm</th>
        <th style="min-width: 200px;">Tổng dự toán (VNĐ)</th>
        <th style="min-width: 250px;">Dự toán sau chiết khấu (VNĐ)</th>
    </tr>
    </thead>
    <tbody>
    @php $i = 0; @endphp
    @foreach($reports as $report)
        <tr>
            <td>{{++$i}}</td>
            @if(isset($report['user_id']) and !empty($report['user_id']))
                <td>{{ $report['user_name'] }}</td>
                <td>{{ $report['user_phone'] }}</td>
                <td>Nhân viên kinh doanh</td>
                <td>{{ number_format($report['quote_number']) }}</td>
                <td>{{ number_format($report['total_product_number']) }}</td>
                <td>{{ number_format($report['total_price']) }}</td>
                <td>{{ number_format($report['discounted_price']) }}</td>
            @else
                <td>{{ $report['customer_name'] }}</td>
                <td>{{ $report['customer_phone'] }}</td>
                <td>Khách hàng</td>
                <td>{{ number_format($report['quote_number']) }}</td>
                <td>{{ number_format($report['total_product_number']) }}</td>
                <td>{{ number_format($report['total_price']) }}</td>
                <td>{{ number_format($report['discounted_price']) }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
