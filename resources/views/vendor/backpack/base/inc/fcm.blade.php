<script src="https://www.gstatic.com/firebasejs/7.10.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.10.0/firebase-messaging.js"></script>
<script>
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyCcciO5MVo7HS47fMlv_YLYdgqf-l29KN4",
        authDomain: "local-969a3.firebaseapp.com",
        databaseURL: "https://local-969a3.firebaseio.com",
        projectId: "local-969a3",
        storageBucket: "local-969a3.appspot.com",
        messagingSenderId: "730654681745",
        appId: "1:730654681745:web:1b5022b27889776728ed30",
        measurementId: "G-9RDHZCQCJY"
    };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();
    messaging.usePublicVapidKey('BMktE4sZ8ILzHwlhauArJ1UodX1YGKTqWwiJq3T_r_qJuNCOdh4THxHLpoO-4IvOdhgeaYeXNbu0yvPNWG1nBrI');

    messaging.onTokenRefresh(() => {
        messaging.getToken().then((refreshedToken) => {
            console.log('Token refreshed.');
            setTokenSentToServer(false);
            sendTokenToServer(refreshedToken);
            resetUI();
        }).catch((err) => {
            console.log('Unable to retrieve refreshed token ', err);
        });
    });
    messaging.onMessage(function (payload) {
        var obj = JSON.parse(payload.data.notification);
        var notification = new Notification(obj.title, {
            icon: obj.icon,
            body: obj.body
        });
    });

    function setTokenSentToServer(sent) {
        window.localStorage.setItem('{{auth()->id()}}', sent ? '1' : '0');
    }

    function isTokenSentToServer() {
        return window.localStorage.getItem('{{auth()->id()}}') === '1';
    }

    function sendTokenToServer(currentToken) {
        console.log(currentToken);
        if (!isTokenSentToServer()) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/subscriptions",
                type: "post",
                data: {'token': currentToken},
                success: function (response) {
                    console.log('Saved token');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
            setTokenSentToServer(true);
        }
    }

    function resetUI() {
        messaging.getToken().then((currentToken) => {
            if (currentToken) {
                sendTokenToServer(currentToken);
            } else {
                setTokenSentToServer(false);
            }
        }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            setTokenSentToServer(false);
        });
    }

    resetUI();
</script>
