<li class="nav-item dropdown pr-2">
  <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
    <img class="img-avatar" src="{{ backpack_avatar_url(backpack_auth()->user()) }}" alt="{{ backpack_auth()->user()->name }}">
  </a>
  <div class="dropdown-menu dropdown-menu-right mr-4 pb-1 pt-1">
      <a href="#" class="dropdown-item" data-toggle="modal" data-target="#myAccount">
          <i class="fa fa-user"></i> {{ trans('backpack::base.my_account') }}
      </a>
      <a href="#" class="dropdown-item" data-toggle="modal" data-target="#changePassword">
          <i class="fa fa-key"></i> {{ trans('backpack::base.change_password') }}
      </a>

    <a class="dropdown-item" href="{{ backpack_url('logout') }}">
        <i class="fa fa-lock"></i> {{ trans('backpack::base.logout') }}</a>
  </div>
</li>
<!-- Modal -->
@include('vendor.backpack.base.my_account')
