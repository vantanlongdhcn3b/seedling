<li data-toggle="tooltip" data-placement="right" title="Sản phẩm" class="nav-item">
    <a class="nav-link" href="{{ backpack_url('seedling') }}">
        <i class="fa fa-cubes fa-2x"></i>
        <i>Cây giống</i>
        <span class="d-lg-none">Sản phẩm</span>
    </a>
</li>
<li data-toggle="tooltip" data-placement="right" title="Phụ kiện" class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('accessory') }}'>
        <i class='fa fa-paperclip fa-2x'></i>
        <i>phụ kiện</i>
        <span class="d-lg-none">Phụ kiện</span>
    </a>
</li>

<li data-toggle="tooltip" data-placement="right" title="Thuộc tính" class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('taxonomy-item') }}'>
        <i class='fa fa-tint fa-2x' aria-hidden="true"></i>
        <span class="d-lg-none">giống cây</span>
    </a>
</li>
<li data-toggle="tooltip" data-placement="right" title="nhóm cây" class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('taxonomy') }}'>
        <i class='fa fa-tint fa-2x' aria-hidden="true"></i>
        <span class="d-lg-none">Nhóm cây </span>
    </a>
</li>
<li data-toggle="tooltip" data-placement="right" title="Danh sách báo giá" class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('quote') }}'>
        <i class='fa fa-star-o fa-lg fa-2x'></i>
        <span class="d-lg-none">Danh sách báo giá</span>
    </a>
</li>

<li data-toggle="tooltip" data-placement="right" title="Báo cáo" class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('report') }}'>
        <i class='fa fa-pie-chart fa-2x'></i>
        <span class="d-lg-none">Báo cáo</span>
    </a>
</li>

<div class="dropdown-menu-bottom">
    <li data-toggle="tooltip" data-placement="right" title="Tài khoản" class='nav-item'>
        <a class='nav-link' href="{{ backpack_url('user') }}">
            <i class="fa fa-user fa-2x" aria-hidden="true"></i>
            <span class="d-lg-none">Tài khoản</span>
        </a>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-cogs fa-2x"></i>
            <span class="d-lg-none">Cài đặt</span>
        </a>
        <div class="dropdown-menu">
            <a data-toggle="tooltip" data-placement="right" title="Cấu hình" class="dropdown-item"
               href='{{ backpack_url('setting') }}'>
                <i class='fa fa-cog fa-lg'></i>
                <span class="d-lg-none">Cấu hình</span>
            </a>
            <div class="dropdown-divider"></div>
            
        </div>
    </li>
</div>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('season') }}'><i class='nav-icon fa fa-question'></i> Seasons</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('supplier') }}'><i class='nav-icon fa fa-question'></i> Suppliers</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('seedling') }}'><i class='nav-icon fa fa-question'></i> Seedlings</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('new_type') }}'><i class='nav-icon fa fa-question'></i> New_types</a></li>