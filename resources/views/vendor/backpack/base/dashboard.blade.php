@extends(backpack_view('blank'))

@section('content')
    <div class="row pt-2">
        <div class="col-sm-6 col-md-6">
            <div class="card mb-2">
                <div class="card-header d-flex justify-content-between">
                    <strong>Quy trình bán hàng</strong>
                </div>
                <img width="100%" alt="Quy trình bán hàng" src="{{ asset('images/defaults/vvcrm-proccess.png') }}">
            </div>
        </div>
        <div class="col-sm-6 col-md-6">
            <div class="card mb-2">
                <div class="card-header d-flex justify-content-between">
                    <strong>Trạng thái đơn hàng</strong>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        @foreach($order_status as $status)
                            <tr style="color: {{ $status['color'] }};background-color: {{ $status['bg_color'] }}">
                                <td><strong>{{ $status['name'] }}</strong></td>
                                <td>{{ $status['count'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        {{--        <div class="col-sm-6 col-md-6">--}}
        {{--            <div class="card mb-2">--}}
        {{--                <div class="card-header d-flex justify-content-between">--}}
        {{--                    <strong>Thông tin bán hàng</strong>--}}
        {{--                    <select onchange="window.location= this.value">--}}
        {{--                        <option value="/admin/dashboard?t=date"--}}
        {{--                                @if(!request()->get('t') || request()->get('t') == 'date')) selected @endif>Hôm nay--}}
        {{--                        </option>--}}
        {{--                        <option value="/admin/dashboard?t=week" @if(request()->get('t') == 'week') selected @endif>Tuần--}}
        {{--                            này--}}
        {{--                        </option>--}}
        {{--                        <option value="/admin/dashboard?t=month" @if(request()->get('t') == 'month') selected @endif>--}}
        {{--                            Tháng này--}}
        {{--                        </option>--}}
        {{--                    </select>--}}
        {{--                </div>--}}
        {{--                 <div class="card-body">--}}
        {{--                    <div class="row">--}}
        {{--                        <div class="col-6 pb-2">--}}
        {{--                            <div class="h3">Nhập hàng</div>--}}
        {{--                            <div class="text-secondary">{{number_format($amount_purchase_now)}} đ/{{number_format($count_product_purchase_now)}} sp--}}
        {{--                            </div>--}}
        {{--                        </div>--}}
        {{--                        <div class="col-6 pb-2 d-flex align-items-center">--}}
        {{--                            @if($amount_purchase_now >= $amount_purchase_last)--}}
        {{--                                <div class="h5 text-success">--}}
        {{--                                    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>--}}
        {{--                                    <span>{{number_format($amount_purchase_last ==0?'0':($amount_purchase_now-$amount_purchase_last)/$amount_purchase_last*100,2)}} %</span>--}}
        {{--                                </div>--}}
        {{--                            @else--}}
        {{--                                <div class="h5 text-danger">--}}
        {{--                                    <i class="fa fa-long-arrow-down" aria-hidden="true"></i>--}}
        {{--                                    <span>{{number_format($amount_purchase_last ==0?'0':($amount_purchase_now-$amount_purchase_last)/$amount_purchase_last*100,2)}} %</span>--}}
        {{--                                </div>--}}
        {{--                            @endif--}}
        {{--                        </div>--}}
        {{--                        <div class="col-6 pb-2">--}}
        {{--                            <div class="h3">Bán hàng</div>--}}
        {{--                            <div class="text-secondary">{{number_format($amount_order_now)}} đ/{{number_format($count_product_order_now)}} sp</div>--}}
        {{--                        </div>--}}
        {{--                        <div class="col-6 pb-2 d-flex align-items-center">--}}
        {{--                            @if($amount_order_now >= $amount_order_last)--}}
        {{--                                <div class="h5 text-success">--}}
        {{--                                    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>--}}
        {{--                                    <span>{{number_format($amount_order_last ==0?'0':($amount_order_now-$amount_order_last)/$amount_order_last*100,2)}} %</span>--}}
        {{--                                </div>--}}
        {{--                            @else--}}
        {{--                                <div class="h5 text-danger">--}}
        {{--                                    <i class="fa fa-long-arrow-down" aria-hidden="true"></i>--}}
        {{--                                    <span>{{number_format($amount_order_last ==0?'0':($amount_order_now-$amount_order_last)/$amount_order_last*100,2)}} %</span>--}}
        {{--                                </div>--}}
        {{--                            @endif--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
    <div class="row pt-2">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <strong>Đơn hàng mới nhất</strong>
                    <div><a href="{{backpack_url('order')}}">Tất cả</a></div>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <thead>
                        <tr class="text-uppercase">
                            <th scope="col">Trạng thái</th>
                            <th scope="col">Ngày tạo</th>
                            <th scope="col">Mã</th>
                            <th scope="col">Nhân viên</th>
                            <th scope="col">Khách hàng</th>
                            <th scope="col">Nguồn KH</th>
                            <th scope="col">Khu vực</th>
                            <th scope="col">Chuyển khoản/Tổng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($orders) == 0)
                            <tr class="text-center">
                                <td colspan="8">Không có đơn hàng!</td>
                            </tr>
                        @else
                            @foreach($orders as $order)
                                <tr>
                                    <td style="color: {{$order['status']['color']}}">{{$order['status']['name']}}</td>
                                    <td>#{{$order['created_at']}}</td>
                                    <td>#{{$order['code']}}</td>
                                    <td>{{$order['user']['name']}}</td>
                                    <td>{{$order['customer']['name']}}</td>
                                    <td>{{ ($order['customer'] and $order['customer']['customer_source_id'])?\App\Models\CustomerSource::query()->find($order['customer']['customer_source_id'])->name:'-'}}</td>
                                    <td>{{ ($order['customer'] and $order['customer']['province_id'])?\App\Models\Province::query()->find($order['customer']['province_id'])->name:'-'}}</td>
                                    <td>{{ \App\Models\Bill::getDiscountAndTotal($order['id'],$order['products']) }} đ</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
