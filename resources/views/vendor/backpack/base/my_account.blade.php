{{-- UPDATE INFO FORM --}}
<div class="modal fade" id="myAccount" tabindex="-1" role="dialog" aria-labelledby="My Account" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-uppercase bg-warning">
                <h5 class="modal-title">{{ trans('backpack::base.update_account_info') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="js-my-account" class="form" action="{{ route('backpack.account.info') }}" method="post">
                <div class="modal-body">
                    <div class="d-none alert alert-danger fade show" role="alert"></div>
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12 form-group">
                            @php
                                $label = trans('backpack::base.name');
                                $field = 'name';
                                $user = auth()->user();
                            @endphp
                            <label class="required">{{ $label }}</label>
                            <input required class="form-control" type="text" name="{{ $field }}"
                                   value="{{ old($field) ? old($field) : $user->$field }}">
                        </div>

                        <div class="col-md-12 form-group">
                            @php
                                $label = config('backpack.base.authentication_column_name');
                                $field = backpack_authentication_column();
                            @endphp
                            <label class="required">{{ $label }}</label>
                            <input required class="form-control"
                                   type="{{ backpack_authentication_column()=='email'?'email':'text' }}"
                                   name="{{ $field }}" value="{{ old($field) ? old($field) : $user->$field }}">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">{{ trans('backpack::base.cancel') }}</button>
                    <button id="js-my-account-click" type="button" class="btn btn-success"><i
                            class="fa fa-save"></i> {{ trans('backpack::base.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- CHANGE PASSWORD FORM --}}
<div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="Change Password"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-uppercase bg-warning">
                <h5 class="modal-title">{{ trans('backpack::base.change_password') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="js-change-password" class="form" action="{{ route('backpack.account.password') }}" method="post">
                <div class="modal-body">
                    <div class="d-none alert alert-danger fade show" role="alert"></div>
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-12 form-group">
                            @php
                                $label = trans('backpack::base.old_password');
                                $field = 'old_password';
                            @endphp
                            <label class="required">{{ $label }}</label>
                            <input autocomplete="new-password" required class="form-control" type="password"
                                   name="{{ $field }}" id="{{ $field }}" value="">
                        </div>

                        <div class="col-md-12 form-group">
                            @php
                                $label = trans('backpack::base.new_password');
                                $field = 'new_password';
                            @endphp
                            <label class="required">{{ $label }}</label>
                            <input autocomplete="new-password" required class="form-control" type="password"
                                   name="{{ $field }}" id="{{ $field }}" value="">
                        </div>

                        <div class="col-md-12 form-group">
                            @php
                                $label = trans('backpack::base.confirm_password');
                                $field = 'confirm_password';
                            @endphp
                            <label class="required">{{ $label }}</label>
                            <input autocomplete="new-password" required class="form-control" type="password"
                                   name="{{ $field }}" id="{{ $field }}" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">{{ trans('backpack::base.cancel') }}</button>
                    <button id="js-change-password-click" type="button" class="btn btn-success"><i
                            class="fa fa-save"></i> {{ trans('backpack::base.change_password') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

