@extends(backpack_view('layouts.plain'))

@section('content')
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-4">
            @if ($errors->has($username))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $errors->first($username) }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card card-logo">
                <div class="p-4"><img class="logo-img" src="/images/defaults/logo.png" alt="logo"></div>
                <div class="card-body p-2">
                    <form class="col-md-12 p-t-10" role="form" method="POST" action="{{ route('backpack.auth.login') }}">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="control-label" for="{{ $username }}"><strong>{{ config('backpack.base.authentication_column_name') }}</strong></label>
                            <div>
                                <input type="text" class="form-control{{ $errors->has($username) ? ' is-invalid' : '' }}" name="{{ $username }}" value="{{ old($username) }}" id="{{ $username }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password"><strong>{{ trans('backpack::base.password') }}</strong></label>
                            <div>
                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ trans('backpack::base.login') }}
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="checkbox col-6">
                                    <label>
                                        <input type="checkbox" name="remember"> {{ trans('backpack::base.remember_me') }}
                                    </label>
                                </div>
                                @if (backpack_users_have_email())
                                    <div class="text-center col-6"><a class="text-white" href="{{ route('backpack.auth.password.reset') }}">{{ trans('backpack::base.forgot_your_password') }}</a></div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if (config('backpack.base.registration_open'))
                <div class="text-center"><a href="{{ route('backpack.auth.register') }}">{{ trans('backpack::base.register') }}</a></div>
            @endif
        </div>
    </div>
@endsection
