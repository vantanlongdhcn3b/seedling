{{-- relationships with pivot table (n-n) --}}
@php
    $results = data_get($entry, $column['name']);
@endphp

<span>
    <?php
        if ($results && $results->count()) {
            $results_array = $results->pluck($column['attribute']);
            echo str_limit(implode(', ', $results_array->toArray()),100);
        } else {
            echo '-';
        }
    ?>
</span>
