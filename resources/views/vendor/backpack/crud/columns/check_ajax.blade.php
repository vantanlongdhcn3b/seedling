{{-- checkbox with loose false/null/0 checking --}}
@php
    $checkValue = data_get($entry, $column['name']);

    $checkedIcon = data_get($column, 'icons.checked', 'fa-check-circle  text-success');
    $uncheckedIcon = data_get($column, 'icons.unchecked', 'fa-circle');

    $exportCheckedText = data_get($column, 'labels.checked', trans('backpack::crud.yes'));
    $exportUncheckedText = data_get($column, 'labels.unchecked', trans('backpack::crud.no'));

    $icon = $checkValue == false ? $uncheckedIcon : $checkedIcon;
    $text = $checkValue == false ? $exportUncheckedText : $exportCheckedText;
@endphp
<div onclick="confirmProductWarehouseDesign(this)"
     data-route="{{ route($column['route_name'],['id'=>$entry->getKey()]) }}" data-button-type="confirm">
<span>
    <i class="fa {{ $icon }} fa-lg"></i>
</span>

    <span class="sr-only">{{ $text }}</span>
</div>
@push('after_scripts') @if ($crud->request->ajax()) @endpush @endif
<script>

    if (typeof confirmProductWarehouseDesign != 'function') {
        $("[data-button-type=confirm]").unbind('click');

        function confirmProductWarehouseDesign(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');
            var row = $("#crudTable a[data-route='" + route + "']").closest('tr');
            console.log(route);
            swal({
                title: "Nhận sản phẩm",
                text: "Xác nhận đã nhận sản phẩm",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "{!! trans('backpack::crud.cancel') !!}",
                        value: null,
                        visible: true,
                        className: "bg-secondary",
                        closeModal: true,
                    },
                    delete: {
                        text: "Xác nhận",
                        value: true,
                        visible: true,
                        className: "bg-success",
                    }
                },
            }).then((value) => {
                if (value) {
                    $.ajax({
                        url: route,
                        type: 'POST',
                        success: function (result) {
                            if(result != 1) {
                                swal({
                                    title: "Thành công",
                                    text: "Xác nhận đã nhận hàng trong kho thiết kế",
                                    icon: "success",
                                    timer: 4000,
                                    buttons: false,
                                });
                                location.reload();
                            }else {
                                swal({
                                    title: "Đã xác nhận",
                                    text: "Đơn hàng, sản phẩm này đã được xác nhận thành không. Không thể xác nhận lại lần 2",
                                    icon: "warning",
                                    timer: 4000,
                                    buttons: false,
                                });
                            }

                        },
                        error: function (result) {
                            swal({
                                title: "Lỗi xác nhận",
                                text: "Xác nhận sản phẩm kho thiết kế không thành công. Vui lòng thử lại",
                                icon: "error",
                                timer: 4000,
                                buttons: false,
                            });
                        }
                    });
                }
            });

        }
    }

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');
</script>
@if (!$crud->request->ajax()) @endpush @endif
