<!-- select2 from array -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label']??'' !!}</label>
    <div class="js-select2-image-field image-zoom">
        <img class="mr-1 zoom zoom-origin-center x6" alt="Product Image" width="50px"
             src="{{ $field['image']??'/images/defaults/default.png'}}">
        <select onchange="loadImage(this)"
                name="{{ $field['name'] }}@if (isset($field['allows_multiple']) && $field['allows_multiple']==true)[]@endif"
                style="width: 100%"
                data-init-function="bpFieldInitSelect2FromArrayElement"
                @include('crud::inc.field_attributes', ['default_class' =>  'form-control select2_from_array'])
                @if (isset($field['allows_multiple']) && $field['allows_multiple']==true)multiple @endif
        >

            @if (isset($field['allows_null']) && $field['allows_null']==true)
                <option value="">{{$field['placeholder']??'-'}}</option>
            @endif


            @if (count($field['options']))
                @foreach ($field['options'] as $value)
                    @if((old(square_brackets_to_dots($field['name'])) && (
                            $value[$field['option_value']][$field['option_key']] == old(square_brackets_to_dots($field['name'])) ||
                            (is_array(old(square_brackets_to_dots($field['name']))) &&
                            in_array($value[$field['option_key']], old(square_brackets_to_dots($field['name'])))))) ||
                            (null === old(square_brackets_to_dots($field['name'])) &&
                                ((isset($field['value']) && (
                                            $value[$field['option_key']] == $field['value'] || (
                                                    is_array($field['value']) &&
                                                    in_array($value[$field['option_key']], $field['value'])
                                                    )
                                            )) ||
                                    (!isset($field['value']) && isset($field['default']) &&
                                    ($value[$field['option_key']] == $field['default'] || (
                                                    is_array($field['default']) &&
                                                    in_array($value[$field['option_key']], $field['default'])
                                                )
                                            )
                                    ))
                            ))
                        <option data-image="{{ $value['image'] }}"
                                data-exp-price="{{ $value['exp_price'] }}"
                                value="{{ $value[$field['option_key']] }}"
                                selected>{{ $value[$field['option_value']] }}</option>
                    @else
                        <option data-image="{{ $value['image'] }}"
                                value="{{ $value[$field['option_key']] }}">{{ $value[$field['option_value']] }}</option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
        <!-- include select2 css-->
        <link href="{{ asset('packages/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" rel="stylesheet"
              type="text/css"/>
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        <!-- include select2 js-->
        <script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
        @if (app()->getLocale() !== 'en')
            <script src="{{ asset('packages/select2/dist/js/i18n/' . app()->getLocale() . '.js') }}"></script>
        @endif
        <script>
            $(document).ready(function () {
                var select = $('select[name=' + '{{ $field['name'] }}');
                var image = '/' + select.children("option:selected").data('image');
                select.prev('img').attr('src', image);

            });
            function loadImage(t) {
                var image = $(t).find(':selected').data('image');
                $(t).prev("img").attr('src', '/' + image);
            }

            function bpFieldInitSelect2FromArrayElement(element) {
                if (!element.hasClass("select2-hidden-accessible")) {
                    element.select2({
                        theme: "bootstrap"
                    }).on('select2:unselect', function (e) {
                        if ($(this).attr('multiple') && $(this).val().length == 0) {
                            $(this).val(null).trigger('change');
                        }
                    });
                }
            }

        </script>
    @endpush

@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
