@extends(backpack_view('layouts.top_left'))

@php
;$defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.add') => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('content')

<div class="d-flex justify-content-center">
	<div class="{{ $crud->getCreateContentClass() }}">
		<!-- Default box -->
        <div class="d-flex justify-content-between align-items-center pl-3 py-2 bg-warning">
            <div class="text-uppercase"><strong>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}</strong></div>
            <a class="btn text-white" href="{{ url($crud->route) }}">
               <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i>
            </a>
        </div>
		@include('crud::inc.grouped_errors')

		  <form method="post" id="needs-validation" novalidate
		  		action="{{ url($crud->route) }}"
				@if ($crud->hasUploadFields('create'))
				enctype="multipart/form-data"
				@endif
		  		>
			  {!! csrf_field() !!}

		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ])
		      @else
		      	@include('crud::form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ])
		      @endif

	          @include('crud::inc.form_save_buttons')
		  </form>
	</div>
</div>
@endsection
