@if($entry->warehouse_status_id != 3 and !isset($entry->delivery_date))
    <a data-toggle="tooltip" data-placement="left" title="Soạn gửi" href="javascript:void(0)"
       onclick="importWarehouse(this)" data-route="{{ url($crud->route.'/'.$entry->getKey().'/delivery') }}"
       class="btn btn-sm" data-button-type="delete"><i class="fa fa-book fa-lg"></i></a>
@else
    <a style="cursor: no-drop" data-toggle="tooltip" data-placement="left" title="Đã tạo soạn gửi" href="javascript:void(0)"
       class="btn btn-sm text-secondary" readonly><i class="fa fa-book fa-lg"></i></a>
@endif
{{-- Button Javascript --}}
{{-- - used right away in AJAX operations (ex: List) --}}
{{-- - pushed to the end of the page, after jQuery is loaded, for non-AJAX operations (ex: Show) --}}
@push('after_scripts') @if ($crud->request->ajax()) @endpush @endif
<script>

    if (typeof importWarehouse != 'function') {
        $("[data-button-type=delete]").unbind('click');

        function importWarehouse(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);

            var route = button.attr('data-route');
            var row = $("#crudTable a[data-route='" + route + "']").closest('tr');
            console.log(route);
            swal({
                title: "Soạn gửi",
                text: "Bạn có chắc chắn muốn soạn gửi sản phẩm này không",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "{!! trans('backpack::crud.cancel') !!}",
                        value: null,
                        visible: true,
                        className: "bg-secondary",
                        closeModal: true,
                    },
                    delete: {
                        text: "Soạn gửi",
                        value: true,
                        visible: true,
                        className: "bg-success",
                    }
                },
            }).then((value) => {
                if (value) {
                    $.ajax({
                        url: route,
                        type: 'get',
                        success: function (result) {
                            swal({
                                title: "Đã tạo soạn gửi",
                                text: "Đã tạo thành công soạn gửi với trạng thái \"Đang soạn gửi\"",
                                icon: "success",
                                timer: 4000,
                                buttons: false,
                            });

                            location.reload();
                        },
                        error: function (result) {
                            swal({
                                title: "Lỗi tạo soạn gửi",
                                text: "Soạn gửi của bạn chưa được tạo. Vui lòng thử lại",
                                icon: "error",
                                timer: 4000,
                                buttons: false,
                            });
                        }
                    });
                }
            });

        }
    }

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');
</script>
@if (!$crud->request->ajax()) @endpush @endif
