@if ($crud->hasAccess('create'))
	<a href="{{ url($crud->route.'/create') }}" class="btn btn-outline-success" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i><span class="d-sm-down-none text-uppercase"> {{ trans('backpack::crud.add') }} {{ $crud->entity_name }}</span></span></a>
@endif
