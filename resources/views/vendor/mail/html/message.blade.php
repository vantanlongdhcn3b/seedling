@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<img height="30px" src="{{ asset('/images/defaults/email-logo.png') }}"/>
@endcomponent
@endslot

{{-- Body --}}
{!! html_entity_decode($slot) !!}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{!! $subcopy !!}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('Đã đăng ký Bản quyền.')
@endcomponent
@endslot
@endcomponent
