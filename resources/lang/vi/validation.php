<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Trường :attribute phải được chấp nhận.',
    'active_url'           => 'Tên miền :attribute không phải là một URL hợp lệ.',
    'after'                => 'Trường :attribute phải sau :date.',
    'after_or_equal'       => 'Trường :attribute phải sau hoặc bằng :date.',
    'alpha'                => 'Trường :attribute chỉ có thể chứa chữ cái.',
    'alpha_dash'           => 'Trường :attribute chỉ có thể chứa chữ cái, số và dấu gạch ngang.',
    'alpha_num'            => 'Trường :attribute chỉ có thể chứa chữ cái và số.',
    'array'                => 'Trường :attribute phải là một mảng.',
    'before'               => 'Trường :attribute phải trước ngày :date.',
    'before_or_equal'      => 'Trường :attribute phải trước hoặc bằng  :date.',
    'between'              => [
        'numeric' => 'Trường :attribute phải ở giữa :min and :max.',
        'file'    => 'Trường :attribute phải ở giữa :min and :max kilobytes.',
        'string'  => 'Trường :attribute phải ở giữa :min and :max characters.',
        'array'   => 'Trường :attribute phải ở giữa :min and :max items.',
    ],
    'boolean'              => 'Trường :attribute phải đúng hoặc sai',
    'confirmed'            => 'Trường :attribute nhận đinh không phù hợp.',
    'date'                 => 'Trường :attribute không phải là ngày hợp lệ.',
    'date_format'          => 'Trường :attribute không khớp với định dạng :format.',
    'different'            => 'Trường :attribute và :other phải khác nhau.',
    'digits'               => 'Trường :attribute phải là :digits.',
    'digits_between'       => 'Trường :attribute phải nằm trong khoảng :min đến :max.',
    'dimensions'           => 'Trường :attribute đã tồn tại',
    'distinct'             => 'Trường :attribute field has a duplicate value.',
    'email'                => 'Trường :attribute không hợp lệ',
    'exists'               => 'Trường :attribute đã chọn không có hiệu lực',
    'file'                 => 'Trường :attribute phải là một tập tin.',
    'filled'               => 'Trường :attribute phải có giá trị.',
    'image'                => 'Trường :attribute phải là một hình ảnh.',
    'in'                   => 'Trường :attribute đã chọn không có hiệu lực.',
    'in_array'             => 'Trường :attribute không tồn tại trong :other.',
    'integer'              => 'Trường :attribute phải là số nguyên.',
    'ip'                   => 'Trường :attribute phải là một địa chỉ IP hợp lệ.',
    'ipv4'                 => 'Trường :attribute phải là một địa chỉ IPv4 hợp lệ.',
    'ipv6'                 => 'Trường :attribute phải là một địa chỉ IPv6 hợp lệ.',
    'json'                 => 'Trường :attribute phải là một chuỗi JSON hợp lệ.',
    'max'                  => [
        'numeric' => 'Trường :attribute không được lớn hơn :max.',
        'file'    => 'Trường :attribute không được lớn hơn :max kilobytes.',
        'string'  => 'Trường :attribute không được lớn hơn :max ký tự.',
        'array'   => 'Trường :attribute không được lớn hơn :max items.',
    ],
    'mimes'                => 'Trường :attribute được hỗ trợ: :values.',
    'mimetypes'            => 'Trường :attribute được hỗ trợ: :values.',
    'min'                  => [
        'numeric' => 'Trường :attribute ít nhất là :min.',
        'file'    => 'Trường :attribute ít nhất là :min kilobytes.',
        'string'  => 'Trường :attribute ít nhất là :min ký tự.',
        'array'   => 'Trường :attribute ít nhất là :min items.',
    ],
    'not_in'               => 'Trường :attribute đã chọn không có hiệu lực.',
    'not_regex'            => 'Trường :attribute định dạng không hợp lệ.',
    'numeric'              => 'Trường :attribute phải là một số.',
    'present'              => 'Trường :attribute là bắt buộc.',
    'regex'                => 'Trường :attribute định dạng không hợp lệ.',
    'required'             => 'Trường  :attribute là bắt buộc.',
    'required_if'          => 'Trường :attribute được yêu cầu khi :other là :value.',
    'required_unless'      => 'Trường :attribute được yêu cầu trừ khi :other trong :values.',
    'required_with'        => 'Trường :attribute được yêu cầu khi :values là present.',
    'required_with_all'    => 'Trường :attribute được yêu cầu khi :values là present.',
    'required_without'     => 'Trường :attribute được yêu cầu khi :values không phải là present.',
    'required_without_all' => 'Trường :attribute bắt buộc khi không có trường :values là present.',
    'same'                 => 'Trường :attribute phải phù hợp với :other.',
    'size'                 => [
        'numeric' => 'Trường :attribute cần phải :size.',
        'file'    => 'Trường :attribute cần phải :size kilobytes.',
        'string'  => 'Trường :attribute cần phải :size ký tự.',
        'array'   => 'Trường :attribute phải chứa :size items.',
    ],
    'string'               => 'Trường :attribute phải là một chuỗi.',
    'timezone'             => 'Trường :attribute phải là một vùng hợp lệ.',
    'unique'               => 'Trường :attribute đã tồn tại.',
    'uploaded'             => 'Trường :attribute không tải lên.',
    'url'                  => 'Trường :attribute định dạng không hợp lệ.',

    'attributes' => [
        'old_password'     => 'mật khẩu cũ',
        'new_password'     => 'mật khẩu mới',
        'confirm_password' => 'nhập lại mật khẩu mới',
        'full_name'        => 'họ và tên',
        'phone'            => 'số điện thoại',
        'password'         => 'mật khẩu',
        'password_confirmation' =>'xác thực mật khẩu',
        'name'             => 'tên',
        'code'             => 'mã sản phẩm'
    ],

];
