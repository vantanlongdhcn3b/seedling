<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Mật khẩu phải có ít nhất sáu ký tự và trùng khớp với mật khẩu xác nhận.',
    'reset'    => 'Mật khẩu của bạn đã được thiết lập lại!',
    'sent'     => 'Chúng tôi đã gửi thông tin lấy lại mật khẩu qua email của bạn!',
    'token'    => 'Mã thông báo đặt lại mật khẩu này không hợp lệ!',
    'user'     => 'Chúng tôi không tìm thấy địa chỉ email của bạn!',
];
