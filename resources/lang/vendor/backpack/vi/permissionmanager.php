<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Permission Manager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Laravel Backpack - Permission Manager
    | Author: Lúdio Oliveira <ludio.ao@gmail.com>
    |
    */
    'name'                  => 'Tên',
    'role'                  => 'vai trò',
    'roles'                 => 'Vai trò',
    'roles_have_permission' => 'Vai trò có quyền truy cập',
    'permission_singular'   => 'Phân quyền',
    'permission_plural'     => 'phân quyền',
    'user_singular'         => 'Tài khoản',
    'user_plural'           => 'Tài khoản',
    'email'                 => 'Email',
    'extra_permissions'     => 'Quyền bổ sung',
    'password'              => 'Mật khẩu',
    'password_confirmation' => 'Xác thực mật khẩu',
    'user_role_permission'  => 'Vai trò và phân quyền',
    'user'                  => 'tài khoản',
    'users'                 => 'tài khoản',
    'guard_type'            => 'Bảo vệ',
    'phone_number'          =>'Số điện thoại',
];
