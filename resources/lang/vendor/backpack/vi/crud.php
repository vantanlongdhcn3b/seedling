<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // Forms
    'save_action_save_and_new'         => 'Lưu và thêm mới',
    'save_action_save_and_edit'        => 'Lưu và chỉnh sửa',
    'save_action_save_and_back'        => 'Lưu và quay lại',
    'save_action_changed_notification' => 'Vị trí mặc định sau khi lưu đã được thay đổi.',

    // Create form
    'add'                 => 'Thêm',
    'back_to_all'         => 'Quay lại ',
    'cancel'              => 'Hủy',
    'add_a_new'           => 'Thêm mới một ',

    // Edit form
    'edit'                 => 'Sửa',
    'save'                 => 'Lưu',

    // Revisions
    'revisions'            => 'Thay đổi',
    'no_revisions'         => 'Không tìm thấy thay đổi nào',
    'created_this'         => 'Đã tạo',
    'changed_the'          => 'Đã thay đổi',
    'restore_this_value'   => 'Khôi phục dự liệu',
    'from'                 => 'từ',
    'to'                   => 'đến',
    'undo'                 => 'Quay lại',
    'revision_restored'    => 'Khổi phục thành công thay đổi',
    'guest_user'           => 'Tài khoản khách',

    // Translatable models
    'edit_translations' => 'Dịch',
    'language'          => 'Ngôn ngữ',

    // CRUD table view
    'all'                       => 'Tất cả ',
    'in_the_database'           => 'trong cơ sở dữ liệu',
    'list'                      => 'Danh sách',
    'actions'                   => 'Hành động',
    'preview'                   => 'Thông tin',
    'delete'                    => 'Xóa',
    'admin'                     => 'Admin',
    'details_row'               => 'Nội dung chi tiết.',
    'details_row_loading_error' => 'Có lỗi tải nội dung chi tiết. Vui lòng thử lại!',
    'clone' => 'Sao chép',
    'clone_success' => '<strong>Sao chéo thành công</strong><br>Một dữ liệu mới đã được thêm vào, với cùng thông tin như mục này.',
    'clone_failure' => '<strong>Sao chép thất bại</strong><br>Các mục mới không được sao chéo. Vui lòng thử lại!',

    // Confirmation messages and bubbles
    'delete_confirm'                              => 'Bạn có chắc chắn muốn xóa dữ liệu này?',
    'delete_confirmation_title'                   => 'Đã xóa',
    'delete_confirmation_message'                 => 'Đã xóa dữ liệu thành công.',
    'delete_confirmation_not_title'               => 'KHÔNG bị xóa',
    'delete_confirmation_not_message'             => "Lỗi! Mục của bạn không bị xóa.",
    'delete_confirmation_not_deleted_title'       => 'Không bị xóa',
    'delete_confirmation_not_deleted_message'     => 'Dữ liệu của bạn không bị xóa.',

    // Bulk actions
    'bulk_no_entries_selected_title'   => 'Không có mục nào được chọn',
    'bulk_no_entries_selected_message' => 'Vui lòng chọn một hoặc nhiều mục để thực hiện một hành động hàng loạt trên chúng.',

    // Bulk confirmation
    'bulk_delete_are_you_sure'   => 'Bạn có chắc chắn muốn xóa những mục sau :number entries?',
    'bulk_delete_sucess_title'   => 'Đã xóa dữ liệu thành công',
    'bulk_delete_sucess_message' => ' mục đã bị xóa',
    'bulk_delete_error_title'    => 'Lỗi! Dữ liệu của bạn không bị xóa.',
    'bulk_delete_error_message'  => 'Không thể xóa một hoặc nhiều mục',

    // Ajax errors
    'ajax_error_title' => 'Lỗi',
    'ajax_error_text'  => 'Lỗi tải dữ liệu. Vui lòng thử lại!',

    // DataTables translation
    'emptyTable'     => 'Không tìm thấy dữ liệu nào!',
    'info'           => 'Hiển thị _START_ đến _END_ trong _TOTAL_ mục',
    'infoEmpty'      => 'Hiển thị 0 đến 0 trong 0 mục',
    'infoFiltered'   => '(lọc từ _MAX_ mục)',
    'infoPostFix'    => '',
    'thousands'      => ',',
    'lengthMenu'     => '_MENU_ Mục',
    'loadingRecords' => 'Đang tải...',
    'processing'     => 'Đang xử lý...',
    'search'         => 'Tìm kiếm ',
    'zeroRecords'    => 'Không tìm thấy dữ liệu nào',
    'paginate'       => [
        'first'    => 'Đầu',
        'last'     => 'Cuối',
        'next'     => 'Trước',
        'previous' => 'Sau',
    ],
    'aria' => [
        'sortAscending'  => ': activate to sort column ascending',
        'sortDescending' => ': activate to sort column descending',
    ],
    'export' => [
        'export'            => 'Export',
        'copy'              => 'Copy',
        'excel'             => 'Excel',
        'csv'               => 'CSV',
        'pdf'               => 'PDF',
        'print'             => 'Print',
        'column_visibility' => 'Column visibility',
    ],

    // global crud - errors
    'unauthorized_access' => 'Unauthorized access - you do not have the necessary permissions to see this page.',
    'please_fix'          => 'Please fix the following errors:',

    // global crud - success / error notification bubbles
    'insert_success' => 'Thêm Thành Công.',
    'update_success' => 'Cập Nhật Thành Công.',

    // CRUD reorder view
    'reorder'                      => 'Kéo thả',
    'reorder_text'                 => 'Sử dụng kéo và thả để sắp xếp lại.',
    'reorder_success_title'        => 'Thành công',
    'reorder_success_message'      => 'Thông tin chỉnh sửa đã được cập nhật.',
    'reorder_error_title'          => 'Lỗi',
    'reorder_error_message'        => 'Thông tin chỉnh sửa chưa được cập nhật.',

    // CRUD yes/no
    'yes' => 'Yes',
    'no'  => 'No',

    // CRUD filters navbar view
    'filters'        => 'Lọc',
    'toggle_filters' => 'Toggle filters',
    'remove_filters' => 'Bỏ lọc',

    // Fields
    'browse_uploads'            => 'Tải lên',
    'select_all'                => 'Tất cả',
    'select_files'              => 'Chọn tệp',
    'select_file'               => 'Chọn tệp',
    'clear'                     => 'Clear',
    'page_link'                 => 'Page link',
    'page_link_placeholder'     => 'http://example.com/your-desired-page',
    'internal_link'             => 'Internal link',
    'internal_link_placeholder' => 'Internal slug. Ex: \'admin/page\' (no quotes) for \':url\'',
    'external_link'             => 'External link',
    'choose_file'               => 'Chọn hình ảnh',

    //Table field
    'table_cant_add'    => 'Cannot add new :entity',
    'table_max_reached' => 'Maximum number of :max reached',

    // File manager
    'file_manager' => 'File Manager',
];
