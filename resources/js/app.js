require('./bootstrap');
window.onload = function () {
    require('./my_account');
    require('./change_password');
    $('[data-toggle="tooltip"]').tooltip();

    var tab = window.location.hash;
    if (tab) {
        $('#myTab a[href="' + tab + '"]').tab('show');
    }
};
