jQuery('#js-my-account-click').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    jQuery.ajax({
        url: $('#js-my-account').attr('action'),
        method: 'post',
        data: $('#js-my-account').serialize(),
        success: function (result) {
            if (result.errors) {
                jQuery.each(result.errors, function (key, value) {
                    jQuery('.alert-danger').removeClass('d-none');
                    jQuery('.alert-danger').html('<li>' + value + '</li>');
                });
            } else {
                $('#myAccount').modal('toggle');
                jQuery('.alert-danger').addClass('d-none');
                swal({
                    title: result.title,
                    text: result.text,
                    icon: "success",
                    timer: 4000,
                    buttons: false,
                });
            }
        }
    });
});
