jQuery('#js-change-password-click').click(function (e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    jQuery.ajax({
        url: $('#js-change-password').attr('action'),
        method: 'post',
        data: $('#js-change-password').serialize(),
        success: function (result) {
            if (result.errors) {
                jQuery.each(result.errors, function (key, value) {
                    jQuery('.alert-danger').removeClass('d-none');
                    jQuery('.alert-danger').html('<li>' + value + '</li>');
                });
            } else {
                $('#changePassword').modal('toggle');
                jQuery('.alert-danger').addClass('d-none');
                swal({
                    title: result.title,
                    text: result.text,
                    icon: "success",
                    timer: 4000,
                    buttons: false,
                });
            }
        }
    });
});
