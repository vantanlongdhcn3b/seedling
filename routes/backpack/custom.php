<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {
    Route::get('dashboard', 'DashboardController@dashboard');
    Route::post('edit-account-info', 'MyAccountController@postAccountInfoForm')->name('backpack.account.info');
    Route::post('change-password', 'MyAccountController@postChangePasswordForm')->name('backpack.account.password');
    Route::crud('user', 'UserCrudController');
    Route::get('setting', 'SettingController@edit');
    Route::crud('taxonomy', 'TaxonomyCrudController');
    Route::crud('taxonomy-item','TaxonomyItemCrudController');
    Route::crud('accessory', 'AccessoryCrudController');
    Route::crud('setting', 'SettingCrudController');
    
    Route::get('report', 'ReportController@index');
    Route::crud('customer', 'CustomerCrudController');
    Route::crud('season', 'SeasonCrudController');
    Route::crud('supplier', 'SupplierCrudController');
    Route::crud('seedling', 'SeedlingCrudController');
    Route::crud('new_type', 'New_typeCrudController');
});