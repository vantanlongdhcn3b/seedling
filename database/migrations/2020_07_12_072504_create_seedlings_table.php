<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeedlingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seedlings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->longText('description')->nullable();
            $table->longText('cultivation_techniques')->nullable();
            $table->string('image')->nullable();

            $table->unsignedBigInteger('user_id');
            
            $table->unsignedBigInteger('season_id')->nullable();
            $table->string('season_name')->nullable();

            $table->unsignedBigInteger('taxonomy_item_id')->nullable();
            $table->string('taxonomy_item_name')->nullable();
            
            $table->unsignedBigInteger('suppier_id')->nullable();
            $table->string('suppier_name');

            $table->tinyInteger('active')->comment('0:not active;1:active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
