<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();
        DB::table('settings')->insert([
            [
                'id' =>1,
                'key' => 'logo_web',
                'name' => 'Logo website',
                'description' => 'Hình ảnh hiển thị ở đầu trang web',
                'value' => null,
                'active' => 1,
                'field' => '{
                     "name":"value",
                     "label":"Logo website",
                     "type":"image",
                     "upload":true,
                     "crop":true,
                     "aspect_ratio":0,
                     "default":"images/defaults/default.png",
                     "max_file_size":0
                }'
            ],
            [
                'id' =>2,
                'key' => 'banner_header_web',
                'name' => 'Banner header website',
                'description' => 'Hình ảnh hiển thị dưới logo website',
                'value' => null,
                'active' => 1,
                'field' => '{
                     "name":"value",
                     "label":"Banner header website",
                     "type":"image",
                     "upload":true,
                     "crop":true,
                     "aspect_ratio": 0,
                     "default":"images/defaults/default.png",
                     "max_file_size":0
                }'
            ],
            [
                'id' =>3,
                'key' => 'header_text_web',
                'name' => 'Header text',
                'description' => 'Nội dung hiển thị dưới banner website',
                'value' => null,
                'active' => 1,
                'field' => '{
                     "name":"value",
                     "label":"Header text",
                     "type":"summernote"
                }'
            ],
            [
                'id' =>4,
                'key' => 'footer_text_web',
                'name' => 'Footer text',
                'description' => 'Nội dung hiển thị ở cuối website',
                'value' => null,
                'active' => 1,
                'field' => '{
                     "name":"value",
                     "label":"Footer text",
                     "type":"summernote"
                }'
            ],
            [
                'id' =>5,
                'key' => 'facebook_link',
                'name' => 'Liên hệ facebook',
                'description' => 'Liên hệ facebook',
                'value' => null,
                'active' => 1,
                'field' => '{
                     "name":"value",
                     "label":"Liên hệ facebook",
                     "type":"text"
                }'
            ],
            [
                'id' =>6,
                'key' => 'zalo_link',
                'name' => 'Liên hệ zalo',
                'description' => 'Liên hệ zalo',
                'value' => null,
                'active' => 1,
                'field' => '{
                     "name":"value",
                     "label":"Liên hệ zalo",
                     "type":"text"
                }'
            ],
            [
                'id' =>7,
                'key' => 'phone_contact',
                'name' => 'Số điện thoại liên hệ',
                'description' => 'Số điện thoại liên hệ',
                'value' => null,
                'active' => 1,
                'field' => '{
                     "name":"value",
                     "label":"Số điện thoại liên hệ",
                     "type":"text"
                }'
            ],
        ]);
    }
}
