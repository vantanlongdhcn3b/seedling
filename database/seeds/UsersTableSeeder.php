<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('model_has_roles')->delete();
        DB::table('users')->insert([
            ['id' => 1, 'name' => 'Tấn Long','phone'=>'0343616713','is_admin'=>1, 'email' => 'vantanlongdhcn3b@gmail.com','password'=>bcrypt('vantanlong1'),'email_verified_at'=>now(), 'remember_token'=>null,'created_at' => now(), 'updated_at' => now()],
            ]);
        DB::table('model_has_roles')->insert([
            ['role_id' => 1, 'model_type' => 'App\Models\BackpackUser', 'model_id' => '1'],
        ]);
    }
}
