<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_types')->delete();
        DB::table('customer_types')->insert([
            ['name' => 'Khách hàng công ty phân','created_at' => now(), 'updated_at' => now()],
            ['name' => 'Khách hàng tự tìm','created_at' => now(), 'updated_at' => now()],
            ['name' => 'Khách hàng đại lý trọn góị','created_at' => now(), 'updated_at' => now()],
            ['name' => 'Khách hàng đại lý giới thiệu','created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
