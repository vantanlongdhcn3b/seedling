<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaxonomiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taxonomies')->delete();
        DB::table('taxonomies')->insert([
            ['id'=>1,'name' => '','created_at' => now(), 'updated_at' => now()],
            ['id'=>2,'name' => '','created_at' => now(), 'updated_at' => now()],
            ['id'=>3,'name' => '','created_at' => now(), 'updated_at' => now()],
            ['id'=>4,'name' => '','created_at' => now(), 'updated_at' => now()],
            ['id'=>5,'name' => '','created_at' => now(), 'updated_at' => now()],
            ['id'=>6,'name' => '','created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
