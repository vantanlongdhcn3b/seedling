<?php

use Illuminate\Database\Seeder;

class TaxonomyItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taxonomy_items')->delete();
        DB::table('taxonomy_items')->insert([
            //Loại cửa
            ['name' => '','taxonomy_id' => 1,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 1,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 1,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 1,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 1,'created_at' => now(), 'updated_at' => now()],
        
            //Số Cánh Cửa
            ['name' => '','taxonomy_id' => 2,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 2,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 2,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 2,'created_at' => now(), 'updated_at' => now()],
            
            //Quy Cách
            ['name' => '','taxonomy_id' => 3,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 3,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 3,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 3,'created_at' => now(), 'updated_at' => now()],
            
            //Loại Kính
            ['name' => '','taxonomy_id' => 4,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 4,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 4,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 4,'created_at' => now(), 'updated_at' => now()],
            
            // Thêm chi tiết khác
            ['name' => '','taxonomy_id' => 5,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 5,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 5,'created_at' => now(), 'updated_at' => now()],
            ['name' => '','taxonomy_id' => 5,'created_at' => now(), 'updated_at' => now()],

            // Dòng sản phẩm
            ['name' => 'Adoor Xingfa','taxonomy_id' => 6,'created_at' => now(), 'updated_at' => now()],
            ['name' => 'Adoor Home',  'taxonomy_id' => 6,'created_at' => now(), 'updated_at' => now()],
        ]);
        
    }
}
