<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        DB::table('products')->insert([
            ['id' =>1,'name' => 'cửa đi 1 cánh mở quay','code' => 'Đ1Q-FIX','description'=>null,'active'=>1,'created_at' => now(), 'updated_at' => now()],
            ['id' =>2,'name' => 'cửa đi 12 cánh mở quay','code' => 'Đ1Q-FIX1','description'=>null,'active'=>1,'created_at' => now(), 'updated_at' => now()],
            ['id' =>3,'name' => 'cửa đi 13 cánh mở quay','code' => 'Đ1Q-FIX2','description'=>null,'active'=>1,'created_at' => now(), 'updated_at' => now()],
            ['id' =>4,'name' => 'cửa đi 14 cánh mở quay','code' => 'Đ1Q-FIX3','description'=>null,'active'=>1,'created_at' => now(), 'updated_at' => now()],
            ['id' =>5,'name' => 'cửa đi 15 cánh mở quay','code' => 'Đ1Q-FIX4','description'=>null,'active'=>1,'created_at' => now(), 'updated_at' => now()],
            ['id' =>6,'name' => 'cửa đi 16 cánh mở quay','code' => 'Đ1Q-FIX5','description'=>null,'active'=>1,'created_at' => now(), 'updated_at' => now()],
            ['id' =>7,'name' => 'cửa đi 117 cánh mở quay','code' => 'Đ1Q-FIX6','description'=>null,'active'=>1,'created_at' => now(), 'updated_at' => now()],
            
        ]);
        
    }
}
