<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('roles')->insert([
            ['id'=>1,'name' => 'Quản trị viên','guard_name' => 'backpack','created_at' => now(), 'updated_at' => now()],
            ['id'=>2,'name' => 'Quản lý','guard_name' => 'backpack','created_at' => now(), 'updated_at' => now()],
            ['id'=>3,'name' => 'Người dùng','guard_name' => 'backpack','created_at' => now(), 'updated_at' => now()],
            ]);
    }
}
