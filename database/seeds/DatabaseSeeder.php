<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SettingSeeder::class);
        
        // $this->call(ProductsSeeder::class);
        // $this->call(TaxonomiesSeeder::class);
        // $this->call(TaxonomyItemsSeeder::class);
    }
}
