<?php

if (!function_exists('str_fcharacter')) {
    //first_character
    function str_fcharacter($string)
    {
        $fcha = null;
        foreach (explode(' ', $string) as $item) {
            $fcha.= strtoupper($item[0]);
        }
        return $fcha;
    }
}
