<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SettingRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SettingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SettingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Setting');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/setting');
        $this->crud->setEntityNameStrings('Cài Đặt', 'Cài Đặt');
    }

    protected function setupListOperation()
    {
        $this->crud->addClause('where', 'active', 1);

        // columns to show in the table view
        $this->crud->setColumns([
            [
                'name'  => 'name',
                'label' => trans('backpack::settings.name'),
            ],
            [
                'name'  => 'value',
                'label' => trans('backpack::settings.value'),
                'type'  => 'closure',
                'priority'=> '3',
                'function' => function ($entry) {
                    if (in_array($entry->key,['logo_web','banner_header_web']))
                        return '<img src="'.asset($entry->value).'" height="50px">';
                    return $entry->value;
                }

            ],
            [
                'name'  => 'description',
                'label' => trans('backpack::settings.description'),
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SettingRequest::class);

        $this->crud->addField([
            'name'       => 'name',
            'label'      => trans('backpack::settings.name'),
            'type'       => 'text',
            'attributes' => [
                'disabled' => 'disabled',
            ],
        ]);

        $this->crud->addField(json_decode($this->crud->getCurrentEntry()->field, true));
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
