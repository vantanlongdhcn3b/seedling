<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class MyAccountController extends Controller
{
    protected $data = [];

    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    public function postAccountInfoForm(Request $request) {
        try {
            $user = backpack_auth()->user();
            $validator = $this->__updateMyAccountValidation($user,$request->all());
            if ($validator)
                return $validator;
            $user->update($request->except(['_token']));
            $this->data['title'] = trans('backpack::base.update_account_info');
            $this->data['text'] = trans('backpack::base.account_updated');
            return response()->json($this->data, 200);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json($e->getMessage(),500);
        }
    }
    public function postChangePasswordForm(Request $request) {
        try {
            $validator = $this->__changePasswordValidation($request->all());
            if ($validator)
                return $validator;
            $user = backpack_auth()->user();
            $user->password = Hash::make($request->new_password);
            $user->save();
            $this->data['title'] = trans('backpack::base.change_password');
            $this->data['text'] = trans('backpack::base.account_updated');
            return response()->json($this->data, 200);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json($e->getMessage(),500);
        }
    }

    private function __updateMyAccountValidation($user,$request)
    {
        $validator = validator()->make($request, [
            backpack_authentication_column() => [
                'required',
                backpack_authentication_column() == 'email' ? 'email' : '',
                Rule::unique($user->getTable())->ignore($user->getKey(), $user->getKeyName()),
            ],
            'name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json(['errors' => $validator->errors()->all()]);
        return false;
    }

    private function __changePasswordValidation($request)
    {
        $validator = validator()->make($request, [
            'old_password'     =>'required',
            'new_password'     => 'required|min:6|different:old_password',
            'confirm_password' => 'required|same:new_password|min:6',
        ]);
        if (!Hash::check($request['old_password'],backpack_auth()->user()->password)){
            $validator->errors()->add('old_password_incorrect', trans('backpack::base.old_password_incorrect'));
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        if ($validator->fails())
            return response()->json(['errors' => $validator->errors()->all()]);
        return false;
    }

}
