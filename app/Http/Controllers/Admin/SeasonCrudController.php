<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SeasonRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SeasonCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SeasonCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    
    public function setup()
    {
        $this->crud->setModel('App\Models\Season');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/season');
        $this->crud->setEntityNameStrings('Thời vụ', 'Thời vụ');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn(
            [
                'name' => 'name',
                'label' => 'Thời vụ',
            ]
        );
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SeasonRequest::class);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Tên thời vụ',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
