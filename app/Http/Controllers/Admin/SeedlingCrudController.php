<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SeedlingRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SeedlingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SeedlingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Seedling');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/seedling');
        $this->crud->setEntityNameStrings('Cây giống', 'Cây giống');
        $this->crud->orderBy('id', 'DESC');
    }

    protected function setupListOperation()
    {

        $this->crud->addColumn(
            [
                'name' => 'image',
                'label' => 'Hình ảnh',
                'type' => 'image',
                'height' => '50px',
                'width' => '50px',
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'code',
                'label' => 'Mã sản phẩm',
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'name',
                'label' => 'Tên sản phẩm',
            ]
        );

        $this->crud->addColumn(
            [
                'name' => 'description',
                'label' => 'Mô tả',
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'cultivation_techniques',
                'label' => 'Kỹ thuật trồng',
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'active',
                'label' => 'Hoạt Động',
                'type' => 'check',
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'suppier_name',
                'label' => 'Nhà cung cấp',
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'taxonomy_item_name',
                'label' => 'Loại cây',
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'season_name',
                'label' => 'Mùa vụ',
            ]
        );
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SeedlingRequest::class);
        //$this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'name',
            'label' => 'Tên sản phẩm',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],

        ]);

        $this->crud->addField([
            'name' => 'code',
            'label' => 'Mã sản phẩm',
            
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],
        ]);
        $this->crud->addField([
            'name' => 'active',
            'label' => 'Hoạt động',
            'type' => 'checkbox',
            'value' => 1,
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],

        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => 'Mô tả',
            'type' => 'summernote',
        ]);
        $this->crud->addField([
            'name' => 'cultivation_techniques',
            'label' => 'Kỹ thuật trồng',
            'type' => 'summernote',
        ]);
        $this->crud->addField([
            'label' => "Hình ảnh",
            'name' => "image",
            'type' => 'image',
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 0,
            'default' => 'images/defaults/default.png',
            'max_file_size' => 0
        ]);

        
        $this->crud->addField([
            'label' => "Loại cây ",
            'type' => 'select2_multiple',
            'name' => 'taxonomy_items', //
            'entity' => 'taxonomy_items',
            'attribute' => 'name',
            
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12',
            ],
            'model' => "App\Models\TaxonomyItem",
            'options' => (function ($query) {

                return $query->orderBy('name', 'ASC')->get();
            }),
        ]);
        $this->crud->addField([
            'label' => "Nhà cung cấp ",
            'type' => 'select2',
            'name' => 'suppier_id', //
            'entity' => 'supplier',
            'attribute' => 'name',
            
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12',
            ],
            'model' => "App\Models\Supplier",
            'options' => (function ($query) {

                return $query->orderBy('name', 'ASC')->get();
            }),
        ]);
        $this->crud->addField([
            'label' => "user name",
            'type' => 'hidden',
            'name' => 'user_id', //
            'value'=>backpack_auth()->user()->id,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12',
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
