<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TaxonomyItemRequest;
use App\Models\Taxonomy;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class Taxonomy_itemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TaxonomyItemCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\TaxonomyItem');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/taxonomy-item');
        $this->crud->setEntityNameStrings('Thuộc Tính', 'Thuộc Tính');
        $this->crud->orderBy('id', 'DESC');
    }

    protected function setupListOperation()
    {
        
        $this->crud->addColumn(
            [
                'name' => 'name',
                'label' => 'Thuộc tính',
            ]
        );
       
        $this->crud->addColumn(
            [
                'name' => 'taxonomy_id',
                'type' => 'select',
                'label' => 'Nhóm Cây',
                'entity' => 'taxonomy',
                'attribute' => 'name',
                'model' => 'App\Models\Taxonomy',
            ]
        );

        $this->crud->addFilter([
            'name' => 'taxonomy_id',
            'type' => 'select2',
            'label' => 'Nhóm thuộc tính'
        ], function () {
            return Taxonomy::query()->pluck('name', 'id')->toArray();
        }, function ($value) {
            $this->crud->addClause('where', 'taxonomy_id', $value);
        });
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TaxonomyItemRequest::class);

        $this->crud->addField([
            'label' => "Nhóm Cây",
            'type' => 'select2',
            'name' => 'taxonomy_id',
            'entity' => 'taxonomy',
            'attribute' => 'name',
            'model' => "App\Models\Taxonomy",
            'default' => 1,
            'options' => (function ($query) {
                return $query->orderBy('id', 'ASC')->get();
            }),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12',
            ],
        ]);
        $this->crud->addField([
            'name' => 'name',
            'label' => 'Tên giống cây',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
