<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TaxonomyRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TaxonomyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TaxonomyCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Taxonomy');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/taxonomy');
        $this->crud->setEntityNameStrings('Nhóm cây', 'Nhóm cây');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn(
            [
                'name' => 'name',
                'label' => 'Nhóm cây',
            ]
        );
        
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TaxonomyRequest::class);
        $this->crud->addFields([
            [
            'name' => 'name',
            'label' => 'Nhóm cây',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                ],
            ],
            
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
