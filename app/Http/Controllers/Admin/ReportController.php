<?php

namespace App\Http\Controllers\Admin;

use App\Exports\QuotesExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quote;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{

    public function index(Request $request)
    {
        $reports = array();
        if ($request->get('date_from') or $request->get('date_to') or $request->get('role')) {
            $reports = $this->getReportQuote($request);
            if ($request->get('export'))
                return Excel::download(new QuotesExport($reports), 'Bao-gia-' . now()->format('d-m-Y') . '.xlsx');
        }
        return view('admin.quote.index', ['reports' => $reports]);
    }

    public function getReportQuote(Request $request)
    {
        $reports = Quote::query()->select(
            'user_id', 'user_name', 'user_phone',
            'customer_id', 'customer_name', 'customer_phone',
            DB::raw('count(id) as quote_number'),
            DB::raw('sum(total_price) as total_price'),
            DB::raw('sum(discounted_price) as discounted_price'),
            DB::raw('sum(total_product_number) as total_product_number'));
        $dateFrom = $request->get('date_from');
        $dateTo = $request->get('date_to');
        $role = $request->get('role');
        if ($dateFrom) {
            $reports = $reports->whereDate('created_at', '>=', $dateFrom);
        }
        if ($dateTo) {
            $reports = $reports->whereDate('created_at', '<=', $dateTo);
        }
        if ($role) {
            switch ($role) {
                case 'sale':
                    $reports = $reports->whereNotNull('user_id');
                    break;
                case 'customer':
                    $reports = $reports->whereNull('user_id');
                    break;
            }
        }
        $reports = $reports->groupBy(['user_id', 'user_name', 'user_phone', 'customer_id', 'customer_name', 'customer_phone'])->get();
        return $reports;
    }
}

