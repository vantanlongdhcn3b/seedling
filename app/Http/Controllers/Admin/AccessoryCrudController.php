<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AccessoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\TaxonomyItem;
/**
 * Class AccessoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AccessoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Accessory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/accessory');
        $this->crud->setEntityNameStrings('Phụ Kiện', 'Phụ Kiện');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn(
            [
                'name'=>'name',
                'label'=>'Tên ',
            ]
        );
        $this->crud->addColumn(
            [
                'name' => 'taxonomy_item_id',
                'type' => 'select',
                'label' => 'Dòng sản phẩm',
                'entity' => 'taxonomyItem',
                'attribute' => 'name',
                'model' => 'App\Models\TaxonomyItem',
            ]
        );

        $this->crud->addFilter([
            'name' => 'taxonomy_item_id',
            'type' => 'select2',
            'label' => 'Dòng sản phẩm',
        ], function () {
            return TaxonomyItem::query()->pluck('name', 'id')->where('taxonomy_id',6)->toArray();
        }, function ($value) {
            $this->crud->addClause('where', 'taxonomy_item_id', $value);
        });

        $this->crud->addColumn(
            [
                'name'=>'description',
                'label'=>'Mô tả',
            ]
        );
        $this->crud->addColumn(
            [
                'name'=>'price',
                'label'=>'Giá',
                'type' => 'number',
                'suffix' => ' VNĐ/Bộ'
            ]
        );
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AccessoryRequest::class);
        $this->crud->addField([
            'label' => 'Dòng sản phẩm',
            'type' => 'select2',
            'name' => 'taxonomy_item_id',
            'entity' => 'taxonomyItem',
            'attribute' => 'name',
            'model' => "App\Models\TaxonomyItem",
            
            'options' => (function ($query) {
                return $query->where('taxonomy_id',6)->orderBy('id', 'ASC')->get();
            }),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12',
            ],
        ]);
        $this->crud->addField([
            'name'=>'name',
            'label'=>'Tên',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],

        ]);
        
        $this->crud->addField([
            'name'=>'price',
            'label'=>'Giá',
            'suffix' => 'VNĐ/Bộ',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        $this->crud->addField([
            'name'=>'description',
            'label'=>'Mô tả',
            'type' => 'summernote',
        ]);
    }
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
