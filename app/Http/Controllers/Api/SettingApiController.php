<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SettingApiController extends Controller
{
    public function index(Request $request) {
        try {
            $data = null;
            if (!$request['keys'])
                return response(config('settings'));
            foreach ($request['keys'] as $key) {
                $data[$key] = config('settings.'.$key);
            }
            return response($data);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return response($e->getMessage(),500);
        }
    }
}
