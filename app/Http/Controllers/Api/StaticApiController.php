<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StaticApiController extends Controller
{
    public function index(Request $request)
    {
        try {
            if (!$request['model'])
                return response('Don\'t have model', 200);
            $data = $request['model']::getAll();
            return response($data, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response($e->getMessage(), 500);
        }
    }
}
