<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class TaxonomyItem extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'taxonomy_items';
     protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['taxonomy_id','name'];
    // protected $hidden = [];
    // protected $dates = [];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($obj) {
            Cache::forget('taxonomy_items');
            Cache::forget('taxonomies');
            Cache::forget('seedlings');
        });
        static::updating(function ($obj) {
            Cache::forget('taxonomy_items');
            Cache::forget('taxonomies');
            Cache::forget('seedlings');
        });
        static::deleting(function ($obj) {
            Cache::forget('taxonomy_items');
            Cache::forget('taxonomies');
            Cache::forget('seedlings');
            \Storage::disk('uploads')->delete(Str::replaceFirst('uploads/', '', $obj->image));
        });
    }
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getAll()
    {
        return Cache::rememberForever('taxonomy_items', function () {
            return static::all()->toArray();
        });
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function taxonomy()
    {
        return $this->belongsTo('App\Models\Taxonomy','taxonomy_id','id');
    }
    public function products()
    {
        return $this->belongsToMany('App\Models\Seedling','product_taxonomy_item','taxonomy_item_id','product_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = 'uploads';
        $destination_path = "texomyItems";

        if ($value == null) {
            \Storage::disk($disk)->delete(Str::replaceFirst('uploads/', '', $this->{$attribute_name}));
            $this->attributes[$attribute_name] = null;
        }

        if (starts_with($value, 'data:image')) {
            $image = \Image::make($value)->encode('jpg', 90);
            $filename = md5($value . time()) . '.jpg';
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            $this->attributes[$attribute_name] = 'uploads/' . $destination_path . '/' . $filename;
        }
    }
}
