<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;

class Seedling extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'seedlings';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['name','code','description','cultivation_techniques','image', 'active','season_name','season_id',
                            'taxonomy_item_name','taxonomy_item_id','suppier_name','suppier_id','user_id'];
    protected $appends = ['taxonomy_items'];
    // protected $hidden = [];
    // protected $dates = [];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($obj) {
            Cache::forget('seedlings');
            Cache::forget('taxonomy_items');
        });
        static::updating(function ($obj) {
            Cache::forget('taxonomy_items');
            Cache::forget('seedlings');
        });
        static::deleting(function ($obj) {
            Cache::forget('taxonomy_items');
            Cache::forget('seedlings');
            \Storage::disk('uploads')->delete(Str::replaceFirst('uploads/', '', $obj->image));
        });
    }
    const ACTIVE = 1;
    
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getList() {
        return Cache::rememberForever('seedlings', function () {
            return static::query()->where('active', self::ACTIVE)->get()->toArray();
        });
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function taxonomyItems()
    {
        return $this->belongsToMany('App\Models\TaxonomyItem','product_taxonomy_item','product_id','taxonomy_item_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier','suppier_id','id');
    }
    public function season()
    {
        return $this->belongsTo('App\Models\Supplier','season_id','id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\Supplier','user_id','id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getTaxonomyItemsAttribute()
    {
        return $this->taxonomyItems()->pluck('taxonomy_item_id');
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = 'uploads';
        $destination_path = "seedlings";

        if ($value == null) {
            \Storage::disk($disk)->delete(Str::replaceFirst('uploads/', '', $this->{$attribute_name}));
            $this->attributes[$attribute_name] = null;
        }

        if (starts_with($value, 'data:image')) {
            $image = \Image::make($value)->encode('jpg', 90);
            $filename = md5($value . time()) . '.jpg';
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            $this->attributes[$attribute_name] = 'uploads/' . $destination_path . '/' . $filename;
        }
    }
}
