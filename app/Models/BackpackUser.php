<?php

namespace App\Models;

use App\User;
use Backpack\CRUD\app\Models\Traits\InheritsRelationsFromParentModel;
use Backpack\CRUD\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;

class BackpackUser extends User
{
    use InheritsRelationsFromParentModel;
    use Notifiable;

    protected $table = 'users';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($obj) {
            Cache::forget('users');
        });
        static::updating(function ($obj) {
            Cache::forget('users');
        });
        static::deleting(function ($obj) {
            Cache::forget('users');
        });
    }

    public static function getAll()
    {
        return Cache::rememberForever('users', function () {
            return static::all()->toArray();
        });
    }

    public static function getUserHasRoleAndPermission() {
        return BackpackUser::query()
            ->leftJoin('model_has_permissions','users.id','=','model_has_permissions.model_id')
            ->leftJoin('model_has_roles','users.id','=','model_has_roles.model_id')
            ->leftJoin('role_has_permissions','model_has_roles.role_id','=','role_has_permissions.role_id');
    }
    public static function getOrderOwner($order_id) {
        return BackpackUser::query()->join('orders','orders.user_id','=','users.id')
            ->where('orders.id',$order_id)->first(['orders.*','users.email','users.name']);
    }
    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    /**
     * Route notifications for the Apn channel.
     *
     * @return string|array
     */
    public function routeNotificationForFcm()
    {
        return $this->push_notifications()->pluck('token')->toArray();
    }

    public function push_notifications() {
        return $this->hasMany(PushSubscription::class, 'subscribable_id','id');
    }
}
