<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class Setting extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'settings';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['value'];
    // protected $hidden = [];
    // protected $dates = [];
    public static function boot()
    {
        parent::boot();

        static::deleting(function ($obj) {
            \Storage::disk('uploads')->delete(Str::replaceFirst('uploads/', '', $obj->image));
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    /**
     * Grab a setting value from the database.
     *
     * @param string $key The setting key, as defined in the key db column
     *
     * @return string The setting value.
     */
    public static function get($key)
    {
        $setting = new self();
        $entry = $setting->where('key', $key)->first();

        if (!$entry) {
            return;
        }

        return $entry->value;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setValueAttribute($value)
    {
        $prefixed_key = null;
        switch ($this->attributes['key']) {
            case 'logo_web':
                $prefixed_key = 'settings.logo_web';
                $this->setImage($value);
                break;
            case 'banner_header_web':
                $prefixed_key = 'settings.banner_header_web';
                $this->setImage($value);
                break;
            case 'header_text_web':
                $prefixed_key = 'settings.header_text_web';
                $this->attributes['value'] = $value;
                break;
            case 'footer_text_web':
                $prefixed_key = 'settings.footer_text_web';
                $this->attributes['value'] = $value;
                break;
            case 'esms_api_key':
                $prefixed_key = 'settings.esms_api_key';
                $this->attributes['value'] = $value;
                break;
            case 'esms_secret_key':
                $prefixed_key = 'settings.esms_secret_key';
                $this->attributes['value'] = $value;
                break;
            default:
                $this->attributes['value'] = $value;
                break;

        }

        // update the value in the session
        Config::set($prefixed_key, $value);

        if (Config::get($prefixed_key) == $value) {
            return true;
        }

        return false;
    }


    public function setImage($value)
    {
        $attribute_name = "value";
        $disk = 'uploads';
        $destination_path = "settings";

        if ($value == null) {
            \Storage::disk($disk)->delete(Str::replaceFirst('uploads/', '', $this->{$attribute_name}));
            $this->attributes[$attribute_name] = null;
        }

        if (starts_with($value, 'data:image')) {
            $image = \Image::make($value)->encode('jpg', 90);
            $filename = md5($value . time()) . '.jpg';
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            $this->attributes[$attribute_name] = 'uploads/' . $destination_path . '/' . $filename;
        }
    }

}
