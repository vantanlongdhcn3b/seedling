<?php
/**
 * @see https://github.com/Edujugon/PushNotification
 */

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAqh5pqpE:APA91bHDpe7OIuahF6QDaYe59xegfBGKQU3HTZbRMoFlorMe4OucbSHumrJ0Cl5CpFB3QwfTSoz7Px_NEgeevo0SQjn2hf1G45jH8sQwFq-OvdMOcoFbEQgkkR3YURGZ-VCSvLOUcSbz',
        'sender_id' => '730654681745'
    ],
    'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAqh5pqpE:APA91bHDpe7OIuahF6QDaYe59xegfBGKQU3HTZbRMoFlorMe4OucbSHumrJ0Cl5CpFB3QwfTSoz7Px_NEgeevo0SQjn2hf1G45jH8sQwFq-OvdMOcoFbEQgkkR3YURGZ-VCSvLOUcSbz',
        'sender_id' => '730654681745'
    ],
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
        'passPhrase' => 'secret', //Optional
        'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
        'dry_run' => true,
    ],
];
