<header class="<?php echo e(config('backpack.base.header_class')); ?>">
  <!-- Logo -->
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a target="_blank" class="navbar-brand" href="<?php echo e(url(config('backpack.base.home_link'))); ?>">
    <?php echo config('backpack.base.project_logo'); ?>

  </a>




  <?php echo $__env->make(backpack_view('inc.menu'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</header>
<?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/base/inc/main_header.blade.php ENDPATH**/ ?>