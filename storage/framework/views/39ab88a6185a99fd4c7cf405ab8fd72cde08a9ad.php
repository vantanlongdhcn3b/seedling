
<div class="modal fade" id="myAccount" tabindex="-1" role="dialog" aria-labelledby="My Account" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-uppercase bg-warning">
                <h5 class="modal-title"><?php echo e(trans('backpack::base.update_account_info')); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="js-my-account" class="form" action="<?php echo e(route('backpack.account.info')); ?>" method="post">
                <div class="modal-body">
                    <div class="d-none alert alert-danger fade show" role="alert"></div>
                    <?php echo csrf_field(); ?>

                    <div class="row">
                        <div class="col-md-12 form-group">
                            <?php
                                $label = trans('backpack::base.name');
                                $field = 'name';
                                $user = auth()->user();
                            ?>
                            <label class="required"><?php echo e($label); ?></label>
                            <input required class="form-control" type="text" name="<?php echo e($field); ?>"
                                   value="<?php echo e(old($field) ? old($field) : $user->$field); ?>">
                        </div>

                        <div class="col-md-12 form-group">
                            <?php
                                $label = config('backpack.base.authentication_column_name');
                                $field = backpack_authentication_column();
                            ?>
                            <label class="required"><?php echo e($label); ?></label>
                            <input required class="form-control"
                                   type="<?php echo e(backpack_authentication_column()=='email'?'email':'text'); ?>"
                                   name="<?php echo e($field); ?>" value="<?php echo e(old($field) ? old($field) : $user->$field); ?>">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal"><?php echo e(trans('backpack::base.cancel')); ?></button>
                    <button id="js-my-account-click" type="button" class="btn btn-success"><i
                            class="fa fa-save"></i> <?php echo e(trans('backpack::base.save')); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="Change Password"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-uppercase bg-warning">
                <h5 class="modal-title"><?php echo e(trans('backpack::base.change_password')); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="js-change-password" class="form" action="<?php echo e(route('backpack.account.password')); ?>" method="post">
                <div class="modal-body">
                    <div class="d-none alert alert-danger fade show" role="alert"></div>
                    <?php echo csrf_field(); ?>

                    <div class="row">
                        <div class="col-md-12 form-group">
                            <?php
                                $label = trans('backpack::base.old_password');
                                $field = 'old_password';
                            ?>
                            <label class="required"><?php echo e($label); ?></label>
                            <input autocomplete="new-password" required class="form-control" type="password"
                                   name="<?php echo e($field); ?>" id="<?php echo e($field); ?>" value="">
                        </div>

                        <div class="col-md-12 form-group">
                            <?php
                                $label = trans('backpack::base.new_password');
                                $field = 'new_password';
                            ?>
                            <label class="required"><?php echo e($label); ?></label>
                            <input autocomplete="new-password" required class="form-control" type="password"
                                   name="<?php echo e($field); ?>" id="<?php echo e($field); ?>" value="">
                        </div>

                        <div class="col-md-12 form-group">
                            <?php
                                $label = trans('backpack::base.confirm_password');
                                $field = 'confirm_password';
                            ?>
                            <label class="required"><?php echo e($label); ?></label>
                            <input autocomplete="new-password" required class="form-control" type="password"
                                   name="<?php echo e($field); ?>" id="<?php echo e($field); ?>" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal"><?php echo e(trans('backpack::base.cancel')); ?></button>
                    <button id="js-change-password-click" type="button" class="btn btn-success"><i
                            class="fa fa-save"></i> <?php echo e(trans('backpack::base.change_password')); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/base/my_account.blade.php ENDPATH**/ ?>