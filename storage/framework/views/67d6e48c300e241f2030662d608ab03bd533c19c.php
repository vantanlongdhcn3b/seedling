<li class="nav-item dropdown pr-2">
  <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
    <img class="img-avatar" src="<?php echo e(backpack_avatar_url(backpack_auth()->user())); ?>" alt="<?php echo e(backpack_auth()->user()->name); ?>">
  </a>
  <div class="dropdown-menu dropdown-menu-right mr-4 pb-1 pt-1">
      <a href="#" class="dropdown-item" data-toggle="modal" data-target="#myAccount">
          <i class="fa fa-user"></i> <?php echo e(trans('backpack::base.my_account')); ?>

      </a>
      <a href="#" class="dropdown-item" data-toggle="modal" data-target="#changePassword">
          <i class="fa fa-key"></i> <?php echo e(trans('backpack::base.change_password')); ?>

      </a>

    <a class="dropdown-item" href="<?php echo e(backpack_url('logout')); ?>">
        <i class="fa fa-lock"></i> <?php echo e(trans('backpack::base.logout')); ?></a>
  </div>
</li>
<!-- Modal -->
<?php echo $__env->make('vendor.backpack.base.my_account', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/base/inc/menu_user_dropdown.blade.php ENDPATH**/ ?>