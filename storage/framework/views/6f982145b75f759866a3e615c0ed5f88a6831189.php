    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="icon" href="<?php echo e(asset('favicon.png')); ?>" sizes="16x16 32x32" type="image/png" />
    <?php if(config('backpack.base.meta_robots_content')): ?><meta name="robots" content="<?php echo e(config('backpack.base.meta_robots_content', 'noindex, nofollow')); ?>"> <?php endif; ?>

    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" /> 
    <title><?php echo e(isset($title) ? $title.' | '.config('backpack.base.project_name') : config('backpack.base.project_name')); ?></title>
<!--  -->
   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<!--  -->
    <?php echo $__env->yieldContent('before_styles'); ?>
    <?php echo $__env->yieldPushContent('before_styles'); ?>

    <?php if(config('backpack.base.styles') && count(config('backpack.base.styles'))): ?>
        <?php $__currentLoopData = config('backpack.base.styles'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $path): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset($path).'?v='.config('backpack.base.cachebusting_string')); ?>">
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>

    <?php if(config('backpack.base.mix_styles') && count(config('backpack.base.mix_styles'))): ?>
        <?php $__currentLoopData = config('backpack.base.mix_styles'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $path => $manifest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(mix($path, $manifest)); ?>">
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css').'/style.css'); ?>">
    <?php echo $__env->yieldContent('after_styles'); ?>
    <?php echo $__env->yieldPushContent('after_styles'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/base/inc/head.blade.php ENDPATH**/ ?>