<?php $__env->startSection('content'); ?>
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-4">
            <?php if($errors->has($username)): ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo e($errors->first($username)); ?>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <div class="card card-logo">
                <div class="p-4"><img class="logo-img" src="/images/defaults/logo.png" alt="logo"></div>
                <div class="card-body p-2">
                    <form class="col-md-12 p-t-10" role="form" method="POST" action="<?php echo e(route('backpack.auth.login')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <label class="control-label" for="<?php echo e($username); ?>"><strong><?php echo e(config('backpack.base.authentication_column_name')); ?></strong></label>
                            <div>
                                <input type="text" class="form-control<?php echo e($errors->has($username) ? ' is-invalid' : ''); ?>" name="<?php echo e($username); ?>" value="<?php echo e(old($username)); ?>" id="<?php echo e($username); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password"><strong><?php echo e(trans('backpack::base.password')); ?></strong></label>
                            <div>
                                <input type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" id="password">
                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-block btn-primary">
                                    <?php echo e(trans('backpack::base.login')); ?>

                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="checkbox col-6">
                                    <label>
                                        <input type="checkbox" name="remember"> <?php echo e(trans('backpack::base.remember_me')); ?>

                                    </label>
                                </div>
                                <?php if(backpack_users_have_email()): ?>
                                    <div class="text-center col-6"><a class="text-white" href="<?php echo e(route('backpack.auth.password.reset')); ?>"><?php echo e(trans('backpack::base.forgot_your_password')); ?></a></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php if(config('backpack.base.registration_open')): ?>
                <div class="text-center"><a href="<?php echo e(route('backpack.auth.register')); ?>"><?php echo e(trans('backpack::base.register')); ?></a></div>
            <?php endif; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(backpack_view('layouts.plain'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/base/auth/login.blade.php ENDPATH**/ ?>