<?php
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
?>

<?php $__env->startSection('content'); ?>
    <div class="d-flex justify-content-center">
	<div class="<?php echo e($crud->getShowContentClass()); ?>">
        <div class="d-flex justify-content-between align-items-center pl-3 py-2 bg-warning">
            <div class="text-uppercase"><strong><?php echo $crud->getSubheading() ?? trans('backpack::crud.preview').' '.$crud->entity_name; ?></strong></div>
            <a class="btn text-white" href="<?php echo e(url($crud->route)); ?>">
                <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i>
            </a>
        </div>
	<!-- Default box -->
	  <div class="">
	  	<?php if($crud->model->translationEnabled()): ?>
	    <div class="row">
	    	<div class="col-md-12 mb-2">
				<!-- Change translation button group -->
				<div class="btn-group float-right">
				  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <?php echo e(trans('backpack::crud.language')); ?>: <?php echo e($crud->model->getAvailableLocales()[$crud->request->input('locale')?$crud->request->input('locale'):App::getLocale()]); ?> &nbsp; <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				  	<?php $__currentLoopData = $crud->model->getAvailableLocales(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $locale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					  	<a class="dropdown-item" href="<?php echo e(url($crud->route.'/'.$entry->getKey().'/show')); ?>?locale=<?php echo e($key); ?>"><?php echo e($locale); ?></a>
				  	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				  </ul>
				</div>
			</div>
	    </div>
	    <?php else: ?>
	    <?php endif; ?>
	    <div class="card no-padding no-border table-responsive">
			<table class="table mb-0">
		        <tbody>
		        <?php $__currentLoopData = $crud->columns(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		            <tr>
		                <td>
		                    <strong><?php echo $column['label']; ?>:</strong>
		                </td>
                        <td>
							<?php if(!isset($column['type'])): ?>
		                      <?php echo $__env->make('crud::columns.text', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		                    <?php else: ?>
		                      <?php if(view()->exists('vendor.backpack.crud.columns.'.$column['type'])): ?>
		                        <?php echo $__env->make('vendor.backpack.crud.columns.'.$column['type'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		                      <?php else: ?>
		                        <?php if(view()->exists('crud::columns.'.$column['type'])): ?>
		                          <?php echo $__env->make('crud::columns.'.$column['type'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		                        <?php else: ?>
		                          <?php echo $__env->make('crud::columns.text', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		                        <?php endif; ?>
		                      <?php endif; ?>
		                    <?php endif; ?>
                        </td>
		            </tr>
		        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php if($crud->buttons()->where('stack', 'line')->count()): ?>
					<tr>
						<td><strong><?php echo e(trans('backpack::crud.actions')); ?></strong></td>
						<td>
							<?php echo $__env->make('crud::inc.button_stack', ['stack' => 'line'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
						</td>
					</tr>
				<?php endif; ?>
		        </tbody>
			</table>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->

	</div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('after_styles'); ?>
	<link rel="stylesheet" href="<?php echo e(asset('packages/backpack/crud/css/crud.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('packages/backpack/crud/css/show.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
	<script src="<?php echo e(asset('packages/backpack/crud/js/crud.js')); ?>"></script>
	<script src="<?php echo e(asset('packages/backpack/crud/js/show.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(backpack_view('layouts.top_left'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/crud/show.blade.php ENDPATH**/ ?>