
<?php
    $results = data_get($entry, $column['name']);
?>

<span>
    <?php
        if ($results && $results->count()) {
            $results_array = $results->pluck($column['attribute']);
            echo str_limit(implode(', ', $results_array->toArray()),100);
        } else {
            echo '-';
        }
    ?>
</span>
<?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/crud/columns/select_multiple.blade.php ENDPATH**/ ?>