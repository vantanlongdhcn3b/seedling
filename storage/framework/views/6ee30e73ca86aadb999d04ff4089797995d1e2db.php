<?php $__env->startSection('content'); ?>
    <div class="nav-header">
        <div class="col-md-4">
            <h5 class="d-flex align-items-center text-success">
                <i class='fa fa-pie-chart fa-2x'></i>
                <strong>BÁO CÁO</strong>
            </h5>
            <?php if(count($reports) > 0): ?>
                <?php $url = url()->full().'&export=true'; ?>
                <a class="btn btn-info" href="<?php echo e($url); ?>">Xuất Excel</a>
            <?php endif; ?>
        </div>
        <div class="col-md-8">
            <form>
                <div class="form-row">
                    <div class="col-md-4 mb-1">
                        <label>Từ ngày</label>
                        <input value="<?php echo e(request()->get('date_from')); ?>" name="date_from" type="date"
                               class="form-control">
                    </div>
                    <div class="col-md-4 mb-1">
                        <label>Đến ngày</label>
                        <input value="<?php echo e(request()->get('date_to')); ?>" name="date_to" type="date" class="form-control">
                    </div>
                    <div class="col-md-2 mb-1">
                        <label>Báo cáo theo</label>
                        <select name="role" class="form-control">
                            <option value="all" <?php if(request()->get('role')=='all'): ?> selected <?php endif; ?>>Tất cả</option>
                            <option value="sale" <?php if(request()->get('role')=='sale'): ?> selected <?php endif; ?>>Nhân viên kinh
                                doanh
                            </option>
                            <option value="customer" <?php if(request()->get('role')=='customer'): ?> selected <?php endif; ?>>Khách
                                hàng
                            </option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label></label>
                        <button type="submit" class="btn btn-success btn-block mt-2">Thống kê</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="p-2 mt-2 bg-white" style="overflow: auto">
                <?php if(count($reports) > 0): ?>
                    <?php echo $__env->make('admin.quote.inc.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php else: ?>
                    <div>Không có dữ liệu!</div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make(backpack_view('layouts.top_left'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/admin/quote/index.blade.php ENDPATH**/ ?>