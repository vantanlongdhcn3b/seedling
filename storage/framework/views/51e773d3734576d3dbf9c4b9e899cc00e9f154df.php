<?php
;$defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.add') => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
?>

<?php $__env->startSection('content'); ?>

<div class="d-flex justify-content-center">
	<div class="<?php echo e($crud->getCreateContentClass()); ?>">
		<!-- Default box -->
        <div class="d-flex justify-content-between align-items-center pl-3 py-2 bg-warning">
            <div class="text-uppercase"><strong><?php echo $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name; ?></strong></div>
            <a class="btn text-white" href="<?php echo e(url($crud->route)); ?>">
               <i class="fa fa-times-circle-o fa-lg" aria-hidden="true"></i>
            </a>
        </div>
		<?php echo $__env->make('crud::inc.grouped_errors', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

		  <form method="post" id="needs-validation" novalidate
		  		action="<?php echo e(url($crud->route)); ?>"
				<?php if($crud->hasUploadFields('create')): ?>
				enctype="multipart/form-data"
				<?php endif; ?>
		  		>
			  <?php echo csrf_field(); ?>


		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      <?php if(view()->exists('vendor.backpack.crud.form_content')): ?>
		      	<?php echo $__env->make('vendor.backpack.crud.form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		      <?php else: ?>
		      	<?php echo $__env->make('crud::form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		      <?php endif; ?>

	          <?php echo $__env->make('crud::inc.form_save_buttons', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		  </form>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(backpack_view('layouts.top_left'), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/crud/create.blade.php ENDPATH**/ ?>