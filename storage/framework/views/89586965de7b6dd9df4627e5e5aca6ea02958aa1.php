<?php if($crud->hasAccess('create')): ?>
	<a href="<?php echo e(url($crud->route.'/create')); ?>" class="btn btn-outline-success" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i><span class="d-sm-down-none text-uppercase"> <?php echo e(trans('backpack::crud.add')); ?> <?php echo e($crud->entity_name); ?></span></span></a>
<?php endif; ?>
<?php /**PATH C:\Users\Admin\Desktop\DoAn\CayGiong\resources\views/vendor/backpack/crud/buttons/create.blade.php ENDPATH**/ ?>